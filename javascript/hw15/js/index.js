const calcFactorial = function (num) {
        if (num !== null || num !== undefined || num !== "" || !isNaN(num)) {
            if (num < 0) {
                return 0;
            } else if (num === 0) {
                return 1;
            } else {
                return num * calcFactorial(num - 1);
            }
        }
};

let number = "";


while (number === null || number === undefined || number == "" || isNaN(number)) {
    number = +prompt("enter the number");
}

console.log(calcFactorial(number));



