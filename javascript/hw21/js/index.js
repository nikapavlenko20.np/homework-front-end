const bth =document.querySelector('.cercle-button');
const btnPrint = document.createElement('button');
const diam = document.createElement('input');
let arr =[];
btnPrint.textContent = 'Нарисовать';

bth.addEventListener('click', event=>{
    event.stopPropagation();
    bth.after(btnPrint);
    bth.after(diam);
    bth.disabled = true;
});

const randomColor = () => {
    const color = `rgba(${Math.random() * 255},${Math.random() * 255}, ${Math.random() * 255})`;
    return color;
};

for(let i=0; i<100; i++){
    let cercle = document.createElement('div');
    cercle.classList.add('cercle');
    cercle.style.backgroundColor = randomColor();
    arr.push(cercle);
}

btnPrint.addEventListener('click', (event)=>{
    event.stopPropagation();
    arr.forEach(element =>{
        btnPrint.after(element);
    });

});

diam.addEventListener('blur', ()=>{
    const cercleItem = document.querySelectorAll('.cercle');
    let diameter = +diam.value;
    cercleItem.forEach(element =>{
        element.style.height =`${+diameter}px`;
        element.style.width =`${+diameter}px`;
    })
});

document.addEventListener('click',event =>{
    if(event.target.classList.contains('cercle')){
        event.target.classList.add('cercle-hidden');
    }else {
        return false;
    }
});



