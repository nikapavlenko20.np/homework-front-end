const table = document.createElement('table');
table.classList.add('table');

for (let i =0; i<20; i++){
    const tr = document.createElement('tr');
    for (let j = 0; j<20; j++){
        const td = document.createElement('td');
        td.classList.add('cell');
        tr.append(td);
    }
    table.append(tr);
}

document.body.prepend(table);

table.addEventListener('click', event=>{
    event.stopPropagation();
    let cellActive = getComputedStyle(event.target);
    if (cellActive.backgroundColor === 'rgb(0, 0, 0)'){
        event.target.classList.remove('cell-active');
    } else {
        event.target.classList.add('cell-active');
    }
});

const changeColor = function (color1, color2){
    const cell = document.querySelectorAll('.cell');
    table.style.backgroundColor = color1;
    cell.forEach(element => {
        let cellStyle = getComputedStyle(element);
        if (cellStyle.backgroundColor === color1){
            element.style.backgroundColor = color2;
        }
    });
};

document.body.addEventListener('click', event =>{
    let tableStyle = getComputedStyle(table);
    if (tableStyle.backgroundColor === 'rgb(0, 0, 0)'){
        changeColor('rgb(255, 255, 255)', 'rgb(0, 0, 0)');
    } else {
        changeColor('rgb(0, 0, 0)', 'rgb(255, 255, 255)');
    }
});
