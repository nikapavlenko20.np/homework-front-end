const bth = document.querySelector('.subscribe-button');
const header = document.head;

if(localStorage.getItem('css' , 'css/styleActive.css') !==null){
    header.querySelector('link[href="css/style.css"]').href = 'css/styleActive.css';
}

bth.addEventListener('click', event =>{
    if(header.querySelector('link[href="css/style.css"]')){
        header.querySelector('link[href="css/style.css"]').href = 'css/styleActive.css';
        localStorage.setItem('css' , 'css/styleActive.css');
    } else {
        header.querySelector('link[href="css/styleActive.css"]').href = 'css/style.css';
        localStorage.clear();
    }
});