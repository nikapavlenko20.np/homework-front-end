$(".navbar-wrap a[href]").on("click", function(){
    $('html').animate({
        scrollTop: $($(this).attr('href')).offset().top-50
    }, 1000);
});

let height = document.documentElement.clientHeight;

$(window).on('scroll', function () {
    if ($(window).scrollTop() > height){
        $('.btn-to-top').addClass('btn-to-top-active ');
    } else {
        $('.btn-to-top').removeClass('btn-to-top-active');
    }
});


$('.btn-to-top').on('click', function () {
    $('html').animate({scrollTop:0}, 500)
});

$('.posts-btn').on('click', function () {
    $('.card-wrap').slideToggle('slow');
});

