const btn = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event)=>{
    btn.forEach(elem => {
        let styleElem = getComputedStyle(elem);
        if (styleElem.backgroundColor === 'rgb(0, 0, 255)'){
            elem.style.backgroundColor = 'black';
        }
        if (elem.textContent.toLowerCase() === event.key.toLowerCase()){
            elem.style.backgroundColor = 'blue';
        }
    });
});