const createNewUser = function () {
    let firstNameUser = prompt('Enter your first name');
    let lastNameUser = prompt('Enter your last name');
    let birthDayUser = prompt('Enter date of birth');

    const newUser = {
        firstName : firstNameUser,
        lastName: lastNameUser,
        birthDay: birthDayUser,
        getAge: function(){
            let now = new Date();
            let age = "";

            age = +(now.getFullYear() - newUser.birthDay.slice(6));

            if (now.getMonth() >= newUser.birthDay.slice(3,5) && now.getDate() >= newUser.birthDay.slice(0,2)){
                return age;
            } else {
                return age-1;
            }
        },
        getLogin: function(){
            let loginName = "";
            loginName = firstNameUser.toLowerCase()[0] + lastNameUser.toLowerCase();
            return loginName;
        },
        getPassword: function(){
            let password = "";
            password = firstNameUser.toUpperCase()[0] + lastNameUser.toLowerCase() + newUser.birthDay.slice(6);
            return password;
        }
    };

    return newUser;
};


const newUser = createNewUser();

console.log(newUser.getAge());

console.log(newUser.getLogin());

console.log(newUser.getPassword());




