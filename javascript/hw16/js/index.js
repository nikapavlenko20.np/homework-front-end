const calcFibonaci = function (n, f0, f1 ) {
    if (checkNumber(n) === true) {
        if (n === 0) {
            return f0;
        } else if (n === 1) {
            return f1;
        } else if (n > 1) {
            return calcFibonaci(n - 2, f0, f1) + calcFibonaci(n - 1, f0, f1);
        } else {
            return calcFibonaci(n + 2 , f0, f1) - calcFibonaci(n + 1 , f0, f1);
        }
    } else {
        return console.log("false");
    }
};

const checkNumber = function (n) {
    return Math.round(n) === n;
};

let number = +prompt("enter the n");
let fib0 = +prompt("enter the f0");
let fib1 = +prompt("enter the f1");


console.log(calcFibonaci(number, fib0, fib1));
