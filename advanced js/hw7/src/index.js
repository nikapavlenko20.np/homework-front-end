const url ={
    urlUsers: 'https://jsonplaceholder.typicode.com/users',
    urlPosts: 'https://jsonplaceholder.typicode.com/posts'
};
const root = document.querySelector('#root');
const addFormForPost = document.querySelector('#create-form');
const titleCreatePost = document.querySelector('#input-title');
const bodyCreatePost = document.querySelector('#input-body');
const bthCreatePost = document.querySelector('#input-create-post');

bthCreatePost.addEventListener('click',postPost);
root.addEventListener('click', actionsWichPots);
addFormForPost.addEventListener('click', getForm);
getPost();



async function fetchData(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

function getForm(){
    document.querySelector('form').classList.toggle('hidden-form');
}

async function postPost(e) {
    e.preventDefault();
    const value= {
        userId: 1,
        title: titleCreatePost.value,
        body: bodyCreatePost.value,
    };
    const dataPost = await postFetch(value);
    const dataUser  = await fetchData(`${url.urlUsers}/1`);
    const post = cteatePost(dataUser,dataPost);
    root.prepend(post);
    document.querySelector('form').classList.add('hidden-form');
}

async function postFetch(value){
    const response = await fetch(url.urlPosts,{
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(value)
    });
    const data = await response.json();
    return data;
}

async function getPost() {
    const users = await fetchData(url.urlUsers);
    const posts = await fetchData(url.urlPosts);
    posts.reverse();
    posts.forEach((post, index) => {
        const userData  = users.find(user => user.id === post.userId);
        const postCreate  = cteatePost(userData, post);
        root.append(postCreate);
    })
}



function cteatePost({name, email},{title, body, id, userId}) {
    const post = document.createElement('div');
    post.classList.add('post');
    post.dataset.postId = +id;
    post.dataset.userId = +userId;
    post.innerHTML = `<p class="post-user-name">${name}</p><p class="post-user-email">${email}</p><h5 class="post-title">${title}</h5><p class="post-body">${body}</p> <button class="delete" type="button">delete</button><button class="edit" type="button">edit</button>`;
    return post;
}

function actionsWichPots(e) {
        if (e.target.classList.contains('delete')) {
            deletePost(e.target.parentElement)
        }
        if (e.target.classList.contains('edit')) {
            editPustOnSite(e.target.parentElement.dataset.postId);
        }
}

async function deletePost(perentElement) {
    const ansver = await deleteDataPost(url.urlPosts, perentElement.dataset.postId);
    if (ansver) perentElement.remove();
};

async function deleteDataPost(url,id) {
    const response = await fetch(`${url}/${id}`, {
        method: "DELETE"
    });
    const answer = response.json();
    if (!answer.length) {
        return true;
    }
    return false;
}

function editPustOnSite(id){
    const perentEl = document.querySelector(`[data-post-id="${id}"]`);
    const title = perentEl.querySelector('.post-title');
    const body = perentEl.querySelector('.post-body');
    title.hidden = true;
    body.hidden = true;
    const div = document.createElement('div');
    div.innerHTML = `<input class="edit-input" value="${title.innerHTML}"><input  class="edit-input" value="${body.innerHTML}"><button class="save">save</button>`;
    perentEl.append(div);
    div.querySelector('button').addEventListener('click',async (e)=>{
        const value= {
            userId: Number(perentEl.dataset.userId),
            title: div.children[0].value,
            body: div.children[1].value
        };
        const answer =await putDataPost(id, value);
        if (answer){
            title.textContent  = div.children[0].value;
            body.textContent  = div.children[1].value;
            div.remove();
            title.hidden = false;
            body.hidden = false;
        }
    } )
}

async function putDataPost(id, value){
    const response = await fetch(`${url.urlPosts}/${id}`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(value)
    });
    const answer = await response.json();
    if (answer) return true;
    return false;
}
