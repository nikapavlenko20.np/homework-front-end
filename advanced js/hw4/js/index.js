const ul = document.querySelector('ul');

async function fetchFilm() {
    const response = await fetch('https://swapi.dev/api/films');
    const dataToJson = await response.json();
    for (let element of dataToJson.results){
        addFilmName(element, ul);
        element.characters.forEach(characters => {
            ftC(characters,element)
        })
    }
}

fetchFilm();

function addFilmName({title, episode_id:  episodeId, opening_crawl: openingCrawl}, perentElement) {
    const li = document.createElement('li');
    li.dataset.id = episodeId;
    li.innerHTML = `<h3>${title}</h3><h5>${episodeId}</h5><p>${openingCrawl}</p><ul></ul>`;
    perentElement.append(li);
}

async function ftC(characters, data) {
    const response = await fetch(characters);
    const dataToJson = await response.json();
    addFilmCharacters(dataToJson, data.episode_id)
}

function addFilmCharacters({name}, id) {
    let rootElement = document.querySelector(`[data-id="${id}"]`);
    let ul = rootElement.querySelector('ul');
    let li = document.createElement('li');
    li.textContent = name;
    ul.append(li);
}