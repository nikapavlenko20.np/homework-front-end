class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
       if (value ===" " || !isNaN(value) || value === undefined){
           console.log(`${value} is not name`);
           return
       }
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (value === " " || isNaN(value) || value === undefined){
            console.log(`${value} is not age`);
            return
        }
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        if (value === " " || isNaN(value) || value === undefined){
            console.log(`${value} is not salary`);
            return
        }
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(...args) {
       let [,,,lang] = args;
        super(...args);
        this._lang = lang;
    }
    get lang(){
        return this._lang;
    }
    set lang(value){
        if (value === " " || !isNaN(value) || value === undefined ){
            console.log(`${value} is not languages`);
            return
        }
        this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const programmer1 = new Programmer('Vlad', 21, 1330, ['java', 'js']);
const programmer2 = new Programmer('Anna', 42, 2000, ['c', 'c++']);
const programmer3 = new Programmer('Vika', 33, 1400, ['js']);
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);