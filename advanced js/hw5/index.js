const IPbth = document.querySelector('button');
IPbth.addEventListener('click', getIPUser);
const  rootElement = document.createElement('ul');
const url = {
    ip: 'https://api.ipify.org/?format=json',
    geo: 'http://ip-api.com/',
};

async function castomGet(url, query="") {
    const response = await fetch(`${url}${query}`);
    const data  =  await response.json();
    return data;
}

async function getIPUser() {
    const dataIP = await castomGet(url.ip);
    const dataLocation = await castomGet(url.geo, `json/${dataIP.ip}?fields=country,regionName,city,district,continent`);
    for (let loc in dataLocation){
        createList(loc,dataLocation[loc]);
    }
    document.body.append(rootElement);
}

async function createList(key, vel) {
    const li = document.createElement('li');
    li.innerHTML = `${key}: ${vel}`;
    rootElement.append(li);
}
