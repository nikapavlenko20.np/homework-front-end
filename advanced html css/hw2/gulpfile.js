const { src, dest, watch } = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const cleancss = require("gulp-clean-css");
const minifyjs =require("gulp-js-minify");
const minifyimg =require("gulp-imagemin");
const clean = require("gulp-clean");

const path = {
    src: {
        server: "./src",
        styles: "./src/scss",
        js: "./src/js",
        img: "./src/img"
    },
    dist: {
        server: "./dist/",
        js: "./dist/js",
    },
};

const serve = function () {
    browserSync.init({
        server: {
            baseDir: './',
        },
        port: 5500,
        browser: "firefox",
    });
};

const img = function () {
    return src(path.src.img + '/*')
        .pipe(minifyimg())
        .pipe(dest('dist/img'));
};

const sassdev = function () {
    return src(path.src.styles + "/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(dest(path.dist.server));
};

const sassprod = function () {
    return src(path.src.styles + "/**/*.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleancss())
        .pipe(concat('styles.min.css'))
        .pipe(dest(path.dist.server));
};

const scriptsdev = function () {
    return src(path.src.server + "/**/*.js")
        .pipe(concat("scripts.min.js"))
        .pipe(dest(path.dist.server));
};

const scriptsprod = function () {
    return src(path.src.server + "/**/*.js")
        .pipe(minifyjs())
        .pipe(concat("scripts.min.js"))
        .pipe(dest(path.dist.server));
};

const cleanAll =function () {
    return src(path.dist.server + "*")
        .pipe(clean());
};

const dev = function(){
    serve();
    sassdev();
    scriptsdev();
    watch("./**/*.html", function (cb) {
        browserSync.reload();
        cb();
    });
    watch(path.src.styles + "/**/*.scss", function (cb) {
        sassdev();
        browserSync.reload();
        cb();
    });
    watch(path.src.js + "/**/*.js", function (cb) {
        scriptsdev();
        browserSync.reload();
        cb();
    });
};

const build = function(){
    cleanAll();
    img();
    sassprod();
    scriptsprod();
};

exports.build = build;
exports.dev = dev;