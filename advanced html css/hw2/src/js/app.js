const menu = document.body.querySelector(".navigation__burger-nav");
const menuList = document.body.querySelector(".navigation__wrap-menu");

menu.addEventListener("click", (event)=>{
    if (!menuList.classList.contains('navigation__wrap-menu--active')){
        menuList.classList.add('navigation__wrap-menu--active')
    }else {
        menuList.classList.remove('navigation__wrap-menu--active')
    }
});