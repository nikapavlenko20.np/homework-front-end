import React, {useEffect} from 'react';
import CardList from "../../components/CardList/CardList";
import {actionsCards, selectCards} from "../../redux/card";
import {useDispatch, useSelector} from "react-redux";

const HomePage = () => {
    const itemsAll = useSelector(selectCards.allCard);
    const dispatch = useDispatch();
    useEffect(() =>{
        dispatch(actionsCards.saveAllEmailsAsync())
    },[]);


    return (
        <>
            <div className='container'>
                <CardList items={itemsAll} />
            </div>
        </>
    );
};

export default HomePage;