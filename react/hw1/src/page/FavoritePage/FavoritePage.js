import React, { useState,useEffect} from 'react';
import CardList from "../../components/CardList/CardList";
import {useDispatch, useSelector} from "react-redux";
import {actionsCards, selectCards} from "../../redux/card";

const FavoritePage = () => {
    const favoriteItems = useSelector(selectCards.favoriteCards);
    const dispatch = useDispatch();
    const [favoriteItemsDelete, setFavoriteItemsDelete] = useState(true);

    useEffect(()=>{
        dispatch(actionsCards.saveCollectionEmailsAsync('favorite'))
    },[]);


    useEffect(()=>{
        dispatch(actionsCards.saveCollectionEmailsAsync('favorite'))
    },[favoriteItemsDelete]);

    return (
        <>
            <div className='container'>
                <CardList renderCard={()=>setFavoriteItemsDelete(!favoriteItemsDelete)} items={favoriteItems}/>
            </div>
        </>
    );
};

export default FavoritePage;