import React, {useEffect, useState} from 'react';
import CardList from "../../components/CardList/CardList";
import {useDispatch, useSelector} from "react-redux";
import {actionsCards, selectCards} from "../../redux/card";
import FormForBuy from "../../components/FormForBuy/FormForBuy";

const BasketPage = () => {
    const [changeArrCard, setChangeArrCard] = useState(false);
    const buyItems = useSelector(selectCards.buyCards);
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(actionsCards.saveCollectionEmailsAsync('buy'))
    },[]);

    useEffect(()=>{
        dispatch(actionsCards.saveCollectionEmailsAsync('buy'))
    },[changeArrCard]);

    return (
        <>
            <div className='container'>
                <CardList renderCard={()=>setChangeArrCard(!changeArrCard)}  items={buyItems}/>
               {buyItems.length!==0 && <FormForBuy/>}
            </div>
        </>
    );
};

export default BasketPage;