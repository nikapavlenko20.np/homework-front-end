import basketBuyReduce, {initialStore} from './reduce';
import type from "./type";


test('basketBuyReduce default test', ()=>{
    const store = {def: '1'};
    const reduser =  basketBuyReduce(store, {type: 'TESTING_REDUCE' });
    expect(reduser).toEqual(store);
});

test(`${type.SAVE_INFORMATION_FOR_BUY} test basketBuyReduce`, ()=>{
    const payloadData = {...initialStore,name: "myname"};
    const reduser =  basketBuyReduce(initialStore, {type: type.SAVE_INFORMATION_FOR_BUY, payload: payloadData});

    expect(reduser).toEqual(payloadData);
});
