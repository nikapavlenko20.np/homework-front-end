import type from './type'
import {actionsCards} from "../card";

const saveDataInFormAction = (data) =>({
    type: type.SAVE_INFORMATION_FOR_BUY,
    payload: data

});

const saveDataInFormActionAsync = (values,resetForm)=>(dispatch)=>{

    console.log(
            `First Name: ${values['First Name']};
            Last Name: ${values['Last Name']};
            Age: ${values['Age']};
            Address: ${values['Address']};
            Phone Number: ${values['Phone Number']};`
    );
    dispatch(saveDataInFormAction(values));
    dispatch(actionsCards.saveCollectionEmailsAsync('buy',[]));
    localStorage['buy'] = JSON.stringify([]);
    resetForm();
};

export default {
    saveDataInForm: saveDataInFormAction,
    saveDataInFormAsync: saveDataInFormActionAsync,
}