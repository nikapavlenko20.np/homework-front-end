import reduce from './reduce';

export {default as actionsForm} from './actions'
export {default as selectorsForm} from './selectors'
export {default as typeActionsForm} from './type'

export default reduce;