import type from "./type";

export const initialStore = {
    firstName: '',
    lastName: '',
    age: null,
    address: '',
    phoneNumber: null,
    buyItem: []
};


const basketBuyReduce = (currentStore = initialStore,action)=>{
    switch (action.type) {
        case type.SAVE_INFORMATION_FOR_BUY:
            return action.payload;
        default:
            return currentStore;
    }
};

export default basketBuyReduce;