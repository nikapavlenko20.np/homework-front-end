import type from './actionTypes';


export const initialStore = {
        all: [],
        favorite: [],
        buy: []
};

 const cardsReducer = (currentStore= initialStore, action)=>{
    switch (action.type) {
        case type.SAVE_ALL_CARDS:
            return {
                ...currentStore,
                all: action.payload
            };
        case type.SAVE_COLLECTIONS_CARDS:
            const {folder, cards} = action.payload;
            return {
                ...currentStore,
                [folder]: cards
            };
        default:
            return currentStore;
    }
};


export default cardsReducer