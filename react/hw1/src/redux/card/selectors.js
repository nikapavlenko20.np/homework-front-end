const selectorAllCards = currentStore => currentStore.cards.all;
const selectorFavoriteCards = currentStore => currentStore.cards.favorite;
const selectorBuyCards = currentStore => currentStore.cards.buy;


export default {
    allCard: selectorAllCards,
    favoriteCards: selectorFavoriteCards,
    buyCards: selectorBuyCards
}