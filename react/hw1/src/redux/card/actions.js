import types from './actionTypes'
import {fetchAllCard, fetchCardsColection} from '../../utils/fetchCards'

const saveAllEmailsAction = (cards)=>({
    type: types.SAVE_ALL_CARDS,
    payload: cards,
});

const saveAllEmailsActionAsync = () => dispatch => {
    fetchAllCard().then(res=> dispatch(saveAllEmailsAction(res)))
};

const saveCollectionEmailsAction = (folder, cards) => ({
    type: types.SAVE_COLLECTIONS_CARDS,
    payload: {
        folder: folder,
        cards: cards,
    }
});

const saveCollectionEmailsActionAsync = (folder )=> dispatch =>{
    fetchCardsColection(folder).then(res => dispatch(saveCollectionEmailsAction(folder, res)));
};

export default {
    saveAllEmails: saveAllEmailsAction,
    saveCollectionEmails: saveCollectionEmailsAction,
    saveCollectionEmailsAsync: saveCollectionEmailsActionAsync,
    saveAllEmailsAsync: saveAllEmailsActionAsync
}


