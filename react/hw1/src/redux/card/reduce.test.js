import cardsReducer, {initialStore} from './reduce';
import type from './actionTypes';


test('cardsReducer reduce default test',()=>{
    const store = {name: 'name'};
    const reduce = cardsReducer(store, {type: 'TESTING_REDUCE'})

    expect(reduce).toEqual(store)
});


test(`${type.SAVE_ALL_CARDS} save test `,()=>{
    const allCard = {id:'222', ...initialStore.all}
    const reduce = cardsReducer(allCard, {type: type.SAVE_ALL_CARDS, payload: allCard})

    expect(reduce).toEqual(allCard)
});

test(`${type.SAVE_COLLECTIONS_CARDS} favorite save test `,()=>{
    const allFavoroteCard = {id:'222', ...initialStore.favorite}
    const reduce = cardsReducer(allFavoroteCard, {type: type.SAVE_COLLECTIONS_CARDS, payload: allFavoroteCard})

    expect(reduce).toEqual(allFavoroteCard)
});