import cards from './card/index'
import modal from './modal/index'
import form from './basket/index'
import {combineReducers} from "redux";

export default combineReducers({
    cards,
    modal,
    form
})