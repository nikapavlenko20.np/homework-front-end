import type from './actionTypes'

export const initialStore = false;

const modalReduce = (currentStore = initialStore,action)=>{
    switch (action.type) {
        case type.TOGGLE_MODAL:
            return action.payload;
        default:
            return currentStore;
    }
};

export default modalReduce;