import modalReduce , {initialStore} from './reduce';
import type from "./actionTypes";


test('modalReduce dafault case', ()=>{
    const reduce = modalReduce(false,{type:'TESTING_REDUCE'});

    expect(reduce).toEqual(false);
});


test(`${type.TOGGLE_MODAL} save test `,()=>{
    const reduce = modalReduce(initialStore, {type: type.TOGGLE_MODAL, payload: true});

    expect(reduce).toEqual(true)
});
