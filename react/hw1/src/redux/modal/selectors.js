const selectorIsModal = currentStore => currentStore.modal;

export default {
    isModalOpen: selectorIsModal
}