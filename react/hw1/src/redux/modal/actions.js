import type from './actionTypes'

const toggleModalOpenActions = (isModalOpen) =>({
    type: type.TOGGLE_MODAL,
    payload: !isModalOpen

});

export default {
    toggleModalOpen: toggleModalOpenActions
}


