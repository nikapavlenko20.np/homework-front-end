import reduce from './reduce';

export {default as actionsModal} from './actions'
export {default as selectorsModal} from './selectors'
export {default as typeActionsModal} from './actionTypes'

export default reduce;