import React from 'react';
import {ErrorMessage} from 'formik';
import './FormForBuy.scss'

const CustomInput = ({form, field, ...rest}) => {
    const {name}= field;

    return (
        <>
                <p className='form__input-title'>{rest.placeholder}</p>
                <input className='form__input' {...field} {...rest}/>
                {form.touched[name]  && form.errors[name] && <span className="form__errors">{form.errors[name]}</span>}

        </>
    );
};

export default CustomInput;