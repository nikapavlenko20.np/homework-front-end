import React from 'react';
import {Formik, Form,Field} from "formik";
import schemas from './valideteShema';
import {actionsForm} from "../../redux/basket";
import {useDispatch, useSelector} from "react-redux";
import {selectCards} from "../../redux/card";
import CustomInput from "./CustomInput";
import './FormForBuy.scss'


const FormForBuy = () => {
    const dispatch= useDispatch();
    const itemBuy = useSelector(selectCards.buyCards);

    const handleSubmit = (values, {resetForm})=>{
        dispatch(actionsForm.saveDataInFormAsync({...values, itemBuy}, resetForm));
    };

    return (
        <Formik
            initialValues={{
            'First Name': '',
            'Last Name': '',
            'Age': '',
            'Address': '',
            'Phone Number': '',
        }}
            onSubmit={handleSubmit}
            validationSchema={schemas.buyFormSchema}
        >
            {props =>
                (<Form className="form" noValidate action="">
                        <Field component={CustomInput}  type='text' name='First Name' placeholder='First Name'/>
                        <Field component={CustomInput} type='text' name='Last Name' placeholder='Last Name'/>
                        <Field component={CustomInput} type='number' name='Age' placeholder='Age'/>
                        <Field component={CustomInput} type='text' name='Address' placeholder='Address'/>
                        <Field component={CustomInput} type='tel' name='Phone Number' placeholder='Phone Number'/>
                        <button className='form__button-submit' type='submit'>Checkout</button>
                    </Form>
                )
            }
        </Formik>
    );
};



export default FormForBuy;