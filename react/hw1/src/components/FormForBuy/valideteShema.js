import * as yup from 'yup';


const buyFormSchema = yup.object().shape({
    'First Name': yup.string().required('this field is required'),
    'Last Name': yup.string().required('this field is required'),
    'Age': yup.number().min(9,'Min age is 9').max(100, 'Max age is 100').required('this field is required'),
    'Address': yup.string().required('this field is required'),
    'Phone Number': yup.number().required('this field is required'),
});

export default {
    buyFormSchema
}