import CardList from "./CardList";
import createMockStore from 'redux-mock-store';
import thunk from "redux-thunk";
import {Provider} from "react-redux";

const store = createMockStore([thunk])({});

jest.mock('react-router-dom', () => ({
    withRouter: Component => props => <Component {...props}/>
}));

jest.mock('../Modal/Modal', ()=>({data})=><div data-testid='modal'>modal</div>);

jest.mock('../Card/Card', ()=>()=> <a>list</a>);


jest.mock("../../redux/modal/index", () => ({
    selectorsModal: {
        isModalOpen: ()=> true,
    }
}));

const items = [{id:'1'},{id: '2'},{id: '3'},{id:'5'}];
test('CardList render',()=>{
    const {getByTestId} = render(<Provider store={store}><CardList location={{pathname: '/'}} items={items}/></Provider>);

    items.forEach((el)=>{
        expect(getByTestId(el.id)).toBeDefined();
    });
});

test('CardList render modal',()=>{
    const {getByTestId} = render(<Provider store={store}><CardList  location={{pathname: '/'}} items={items}/></Provider>);
    expect(getByTestId('modal')).toBeDefined();
});