import React,{useState} from 'react';
import Card from "../Card/Card";
import {withRouter} from "react-router-dom";
import './CardList.scss';
import PropTypes from 'prop-types'
import Modal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {actionsModal, selectorsModal} from "../../redux/modal";



export const CardList =({items, location, renderCard, })=> {
    const isModalOpen = useSelector(selectorsModal.isModalOpen);
    const dispatch = useDispatch();
    const [currentCardID, setCurrentCardID] = useState('');

    const handleClickForCloseModal=()=>{
        dispatch(actionsModal.toggleModalOpen(isModalOpen))
    };

    const handelClickForGetCurrentCard=(e,id)=>{
        handleClickForCloseModal();
        setCurrentCardID(id);
    };

    const itemsArray = items.map((card)=>(
        <li key={card.id} data-testid={card.id} className='cards__item'>
            <Card  place={location.pathname}  id={card.id} changeFavorites={()=>renderCard()} clickOnBth={(e,id)=>handelClickForGetCurrentCard(e,id)} name={card.name} price={card.price} img={card.img} about={card.about} color={card.color}/>
        </li>));

        return (
            <>
                <ul className='cards__list'>
                    {itemsArray}
                </ul>
                     {isModalOpen && <Modal data-testid='modal' place={location.pathname} renderCard={()=>renderCard()} header='item added to cart' closeButton={false} text='do you want to continue shopping' currentIdCard={currentCardID} closeModal={()=>handleClickForCloseModal()}/>}
            </>
        )
};

export default withRouter(CardList);

const EmailElement = PropTypes.shape(
    {
        id: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.string,
        img: PropTypes.string,
        color: PropTypes.string,
    }
);

CardList.propTypes = {
    items: PropTypes.arrayOf(EmailElement)
};