import Header from "./Header";

jest.mock('react-router-dom', () => ({
    Link: 'a'
}));

test('Footer rendered', () => {
    const items= [1,2,3];
    const {getByText}= render(<Header item={items}/>);

    items.forEach((el)=>{
        expect(getByText(el)).toBeDefined();
    })
});

test('Footer rendered without props', () => {
    render(<Header/>);
});


