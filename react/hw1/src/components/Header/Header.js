import React from 'react';
import '../../App.scss';
import './Header.scss'
import {Link} from "react-router-dom";

const Header = ({item=[]}) => {



    return (
        <header className='header'>
            <div className='container'>
                <nav className='header__nav-wrapper'>
                    <ul className='header__nav-list'>
                        {
                            item.map((el,k)=> (
                                <li key={k} className='header__nav-item'><Link className='header__nav-link' to='/'>{el}</Link></li>
                            ))
                        }
                        {/**/}
                        {/*<li className='header__nav-item'><Link className='header__nav-link' to='/basket'>basket</Link></li>*/}
                        {/*<li className='header__nav-item'><Link className='header__nav-link' to='/favorites'>favorites</Link></li>*/}
                    </ul>
                </nav>
            </div>
        </header>
    );
};

export default Header;