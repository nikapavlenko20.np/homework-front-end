import React from 'react';
import './Modal.scss'
import Button from "../Button/Button";
import PropTypes from "prop-types";


const Modal  = ({header,closeButton, text, place,closeModal, currentIdCard, renderCard} )=>{
    const handelClick =(e)=>{
       if (e.target.className === 'module__container-body'){
           closeModal();
       }
    };

    const handelClickForBuyCardItem= ()=>{
        closeModal();
        if(place ==='/basket'){
            let arrBuyCard = JSON.parse(localStorage['buy']);
            const index = arrBuyCard.indexOf(currentIdCard);
            if (index>-1){
                arrBuyCard.splice(index,1);
            }
            localStorage['buy'] = JSON.stringify(arrBuyCard);
            renderCard();
            return;
        }
        let arrBuy = [];
        if ( !!localStorage['buy'] && localStorage['buy'].length!==0) {
            arrBuy =  JSON.parse(localStorage['buy']);
        } else {
            localStorage.setItem('buy', arrBuy);
        }
        arrBuy.push(currentIdCard);
        localStorage['buy'] = JSON.stringify(arrBuy);
    };

        return (
            <div onClick={(e)=>handelClick(e)} className='module__container-body'>
                <div className='module__container'>
                    <div className='module__header'>
                        <h3  className='module__title'>{header}</h3>
                        {closeButton && <span data-testid="close" className='module__close' onClick={()=>closeModal()}>x</span>}
                    </div>
                    <div className='module__body'>
                        <p className='module__content'>{text}</p>
                        <Button backgroundColor='#FF5F4D' text='Close' handleClickOnBth={()=>closeModal()} id='sdfsss'/>
                        <Button backgroundColor='#FF5F4D' text='Ok' handleClickOnBth={()=>handelClickForBuyCardItem()} id='sdfs2222sss'/>
                    </div>
                </div>
            </div>
        );
}

export default Modal;

Modal.defaultProps = {
    closeButton: false
}


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeModal: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    closeButton: PropTypes.bool
};

