import Modal from "./Modal";

jest.mock('../Button/Button', ()=>()=><button>button</button>);

test('Modal render', ()=>{
  const {getByText, getAllByText}=render(<Modal text='test text' header='test title' closeModal={()=>null}/>);

  const title = getByText('test title');
  const text = getByText('test text');

  expect(title.textContent).toBe('test title');
  expect(text.textContent).toBe('test text');
  expect(getAllByText('button')).toBeDefined();
});

test('Modal render without props', ()=>{
    render(<Modal header='test header' closeModal={()=>null} text='test text'/>);
});

test('Modal render with closeButton', ()=>{
    const {getByTestId} = render(<Modal header='test header' closeModal={()=>null} text='test text' closeButton={true}/>);
    expect(getByTestId('close')).toBeDefined();
});

