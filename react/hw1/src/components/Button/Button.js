import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types'

const Button = ({backgroundColor, text, handleClickOnBth,id} )=>{
        return (
            <button data-testid={'bth'} className='button' key={id} style ={{backgroundColor: backgroundColor}} onClick={(e)=>handleClickOnBth(e)}>{text}</button>
        );
};

export default Button;

Button.defaultProps = {
    backgroundColor: 'white',
    text: '',
    handleClickOnBth: ()=>null,
    id: ''
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    handleClickOnBth: PropTypes.func,
    id: PropTypes.string
}