import Button from "./Button";
import {fireEvent} from "@testing-library/react";

test('Button render',()=>{
    const {getByText}=render(<Button backgroundColor='red' text='test' handleClickOnBth={()=>null} id='1'/>);
    expect(getByText('test')).toBeDefined();
    expect(getByText('test')).toHaveAttribute('style', 'background-color: red;');
});

test('Button render without props',()=>{
    render(<Button/>);
});

test('click on  Button function', ()=>{
    const clickBthMock = jest.fn();
    const {getByTestId}= render(<Button handleClickOnBth={clickBthMock}/>);
    const bth = getByTestId('bth');

    expect(bth).toBeDefined();

    fireEvent.click(bth);
    expect(clickBthMock).toBeCalled();
})

