import Card from "./Card";
import {fireEvent} from '@testing-library/react'


let propsForCard = {
    name: 'Oh my Deer',
    price: '12',
    img: '',
    color: 'red',
    about: 'about',
    id: '1',
    place: 'home',
};
test('Card render whith props', ()=>{
    const {getByText, container}=render(<Card  {...propsForCard}/>);
    expect(getByText('about')).toBeDefined();
    expect(getByText('Oh my Deer')).toBeDefined();
    expect(getByText('12')).toBeDefined();
    expect(container.querySelector('[data-testid="bth--delete"]')).toBeNull();
    expect(container.querySelector('[data-testid="bth-add-in-basket"]')).toBeDefined();
   expect(container.querySelector('.card__wrapper')).toHaveAttribute('style', 'color: red;');
});

test('Card render whithout props', ()=>{
    render(<Card/>);
});

test('Card  not to be in basket', ()=>{
    const deleteCardMock = jest.fn();
    const {getByTestId, container} = render(<Card place='/basket' clickOnBth={deleteCardMock}/>);
    const deleteBth = getByTestId('bth--delete');

    expect(container.querySelector('[data-testid="bth-add-in-basket"]')).toBe(null);

    expect(deleteBth).toBeDefined();
    fireEvent.click(deleteBth);
    expect(deleteCardMock).toBeCalled();
});

test('Card  click on favorite', ()=>{
    const favoriteCardMock = jest.fn();
    const {getByTestId} = render(<Card clickOnFavoriteTest={favoriteCardMock}/>);
    const favoriteBth = getByTestId('bth--favorite');

    expect(favoriteBth).toBeDefined();

    fireEvent.click(favoriteBth);
    expect(favoriteCardMock).toBeCalled();
});

