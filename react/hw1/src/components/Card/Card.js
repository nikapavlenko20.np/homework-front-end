import React, {useState} from 'react';
import './Card.scss';
import PropTypes from 'prop-types';

const Card = ({name, price, img, color, about,id, changeFavorites, place, clickOnBth, clickOnFavoriteTest})=> {
    let favoriteStarsButton = 'img/icon-star-not-fav.png';
    const [isFavorite, setIsFavorite] = useState(false);

    const handleClickOnFavorite = ()=>{
        const index = arrFavorite.indexOf(id);
        if (index > -1){
            arrFavorite.splice(index, 1);
            favoriteStarsButton = 'img/icon-star-not-fav.png';
        } else {
            arrFavorite.push(id);
            favoriteStarsButton = 'img/icon-star-fav.png';
        }
        if(place ==='/favorites') changeFavorites();
        localStorage['favorite'] = JSON.stringify(arrFavorite);
        setIsFavorite({
            isFavorite: !isFavorite,
        })
    };

    let arrFavorite = [];
    if ( !!localStorage['favorite'] ) {
        arrFavorite = JSON.parse(localStorage['favorite']);
    } else {
        arrFavorite=[];
        localStorage.setItem('favorite', arrFavorite);
    }
        const index = JSON.parse(localStorage['favorite'].indexOf(id));
        if (index > -1) {
            favoriteStarsButton = 'img/icon-star-fav.png';
        }

        return (
            <div data-id={id} className='card__wrapper' style={{color: color}}>
                {place==='/basket' && <button data-testid="bth--delete" className='card__bth--delete' onClick={(e)=>clickOnBth(e,id)} key="0001">X</button>}
                <div className='card__img-wrapper'>
                    <img className='card__img' src={img} alt=""/>
                </div>
                <h3 className='card__book-name'>{name} <span className='card__book-author'>by author</span></h3>
                <button data-testid="bth--favorite" key='0002' className='card__bth-favorite' onClick={()=>{clickOnFavoriteTest() || handleClickOnFavorite(id)}}><img className='card__fav-img'
                    src={favoriteStarsButton} alt="stars"/></button>
                <p className='card__book-desc'>{about}</p>
                <div className='card__bay-info'>
                    <p className='card__prise'>{price}</p>
                    {place!=='/basket' && <button key='0003' data-testid="bth-add-in-basket" className='card__bth' onClick={(e)=>clickOnBth(e,id)}>add to card</button>}
                </div>
            </div>
        );
}

export default Card;

Card.defaultProps = {
    name: '',
    price: '',
    img: '',
    color: 'black',
    about: '',
    id:'',
    changeFavorites: ()=>null,
    place:'',
    clickOnBth:()=>null
};

Card.propTypes = {
    name: PropTypes.string.isRequired,
    price:  PropTypes.string.isRequired,
    img:  PropTypes.string.isRequired,
    color:  PropTypes.string,
    about:  PropTypes.string,
    id: PropTypes.string.isRequired,
    changeFavorites:  PropTypes.func,
    place:  PropTypes.string,
    clickOnBth: PropTypes.func
};