import React from 'react';
import {Route, Switch} from "react-router-dom";
import HomePage from "../page/HomePage/HomePage";
import FavoritePage from "../page/FavoritePage/FavoritePage";
import BasketPage from "../page/BasketPage/BasketPage";

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path="/" component={HomePage}/>
            <Route path="/favorites" component={FavoritePage}/>
            <Route exact path="/basket" component={BasketPage}/>
        </Switch>
    );
};

export default AppRoutes;