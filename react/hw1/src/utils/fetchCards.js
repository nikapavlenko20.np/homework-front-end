 export const fetchCardsColection = (nameStorage)=>
    (fetch('./data.json').then(res=>res.json()).then(data=>{
             const arrLocalStorage = localStorage[nameStorage];
            const arrCurrentCard = [];
             if (arrLocalStorage) {
                 for (let currentCardID of arrLocalStorage) {
                     const item = data.content.find(card => card.id === currentCardID);
                     if (item) {
                         arrCurrentCard.push(item);
                     }
                 }
             }
            return arrCurrentCard;
        })
);

export const fetchAllCard = ()=> (
     fetch('./data.json')
        .then(res=> res.json()).then(res=> res.content)
);