import App from './App';

jest.mock('./components/Header/Header', () => ()=> <div>header</div>);
jest.mock('./rourer/AppRoutes', () => ()=> <div>page</div>);

test('renders learn react link', () => {
  const {getByText}=render(<App />);
  expect(getByText('header')).toBeDefined();
  expect(getByText('page')).toBeDefined();
});
