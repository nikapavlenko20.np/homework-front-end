import React from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import AppRoutes from "./rourer/AppRoutes";

 const App = ()=>{

    return (
            <>
                <Header item={['home','basket','favorites']}/>
                <AppRoutes/>
            </>
        );
}

export default App;
